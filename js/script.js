function createNewUser() {
  let newUser = {
      firstName: prompt('Enter your firsname:'),
      lastName: prompt('Enter your lastname:'),
      birthday: prompt('Enter your birthday (dd.mm.yyyy):'),

      getLogin() {
          return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      },

      getAge() {
          const today = new Date();
          const birthDate = new Date(this.birthday.split('.').reverse().join('.'));
          let age = today.getFullYear() - birthDate.getFullYear();
          const month = today.getMonth() - birthDate.getMonth();
          if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
              age--;
          }
          return age;
      },

      getPassword() {
          const birthYear = this.birthday.split('.')[2];
          return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthYear;
      }
  };
  return newUser;
}

const newUser = createNewUser();
console.log("User login:", newUser.getLogin());
console.log("User age:", newUser.getAge());
console.log("User password:", newUser.getPassword());
